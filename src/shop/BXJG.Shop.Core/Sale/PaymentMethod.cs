﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.Shop.Sale
{
    /// <summary>
    /// 商城订单的支付方式
    /// </summary>
    public enum PaymentMethod
    {
        /// <summary>
        /// 微信支付
        /// </summary>
        WeChat
    }
}
