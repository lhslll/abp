﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BXJG.Common
{
    //常用枚举

    public enum Gender
    {
        [Description("女")]
        Woman = 2,
        [Description("男")]
        Man = 1
    }
}
