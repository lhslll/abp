﻿using Abp.Application.Services;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.MultiTenancy;
using BXJG.GeneralTree;
using BXJG.Shop.Customer;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using BXJG.Shop.Catalogue;
using Abp.Threading;
using BXJG.Common;
using ZLJ.BaseInfo.Administrative;
using Microsoft.EntityFrameworkCore;

namespace BXJG.Shop.Sale
{
    /// <summary>
    /// 前台顾客对订单的操作接口
    /// </summary>
    public class CustomerOrderAppService : CustomerAppServiceBase, ICustomerOrderAppService
    {
        private readonly IRepository<OrderEntity, long> repository;
        private readonly OrderManager orderManager;
        private readonly IRepository<AdministrativeEntity, long> generalTreeManager;
        private readonly IRepository<ProductEntity, long> itemRepository;

        public ICancellationTokenProvider CancellationToken { get; set; } = NullCancellationTokenProvider.Instance;

        public CustomerOrderAppService(IRepository<CustomerEntity, long> customerRepository,
                                       CustomerManager customerManager,
                                       ICustomerSession customerSession,
                                       IRepository<OrderEntity, long> repository,
                                       OrderManager orderManager,
                                       IRepository<AdministrativeEntity, long> generalTreeManager,
                                       IRepository<ProductEntity, long> itemRepository) : base(customerRepository, customerManager, customerSession)
        {
            this.repository = repository;
            this.orderManager = orderManager;
            this.generalTreeManager = generalTreeManager;
            this.itemRepository = itemRepository;
        }

        /// <summary>
        /// 创建订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<CustomerOrderDto> CreateAsync(CustomerOrderCreateDto input)
        {
            var customer = await base.GetCurrentCustomerAsync();
            var area = await generalTreeManager.GetAsync(input.AreaId);
            var itemIds = input.Items.Select(c => c.ProductId).ToArray();
            var items = await itemRepository.GetAllIncluding(c => c.Skus).Where(c => itemIds.Contains(c.Id)).ToListAsync();
            var itemEntities = new List<OrderItemInput>();
            foreach (var item in input.Items)
            {
                var product = items.Single(c => c.Id == item.ProductId);
                SkuEntity sku = null;
                if (item.SkuId.HasValue)
                    sku = product.Skus.Single(c => c.Id == item.SkuId);
                itemEntities.Add(new OrderItemInput(product, sku, item.Quantity));
            }
            var order = await orderManager.CreateAsync(
                customer,
                area,
                input.Consignee,
                input.ConsigneePhoneNumber,
                input.ReceivingAddress,
                input.CustomerRemark,
                itemEntities.ToArray());
            return ObjectMapper.Map<CustomerOrderDto>(order);
        }
        ///// <summary>
        ///// 前台顾客发起订单支付
        ///// </summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //public async Task<CustomerPaymentResult> PaymentAsync(CustomerPaymentInput input)
        //{
        //    var customerId = await base.GetCurrentCustomerIdAsync();
        //    var order = await repository.GetAsync(input.OrderId);
        //    if (customerId != order.CustomerId)
        //        throw new ApplicationException();

        //  //  WeChatPaymentUnifyOrderResult wpor = await weChatPaymentService.PayAsync("ABP-商城", order.OrderNo, order.PaymentAmount,cancellationToken:this.CancellationToken.Token);
        //    return new CustomerPaymentResult(null);
        //}
    }
}
